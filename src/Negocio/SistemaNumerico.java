/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import util.ufps.colecciones_seed.ListaCD;

/**
 * EN ESTÁ CLASE SOLO SE PUEDEN UTILIZAR ITERADORES
 *
 * @author madarme
 */
public class SistemaNumerico {

    //contendrá dos listas 
    private ListaCD<ListaCD<Integer>> listas;

    public SistemaNumerico() {
    }

    /**
     *
     * @param veces cantidad de elementos para lista 1
     * @param n inicio de random para lista 1
     * @param m fin de random para lista 1
     * @param veces2 cantidad de elementos para lista 2
     * @param n2 inicio de random para lista 2
     * @param m2 fin de random para lista 2
     */
    public SistemaNumerico(int veces, int n, int m, int veces2, int n2, int m2) {

        //:)
    }

    /**
     *
     * @return Una multilista (una lista circular donde cada elemento es otra
     * lista circular)
     */
    public ListaCD<ListaCD<Integer>> getOperaciones() {
        ListaCD<ListaCD<Integer>> total = new ListaCD();
        total.insertarFin(this.getUnion());
        total.insertarFin(this.getInterseccion());
        total.insertarFin(this.getDiferencia());
        total.insertarFin(this.getDiferenciaSimetrica());
        return total;
    }

    /**
     *
     * @return una lista con la unión de conjuntos de l1 con l2
     */
    public ListaCD<Integer> getUnion() {

        return null;
    }

    /**
     *
     * @return una lista con la Interseccion de conjuntos de l1 con l2
     */
    public ListaCD<Integer> getInterseccion() {

        return null;
    }

    /**
     *
     * @return una lista con la Diferencia de conjuntos de l1 con l2
     */
    public ListaCD<Integer> getDiferencia() {

        return null;
    }

    /**
     *
     * @return una lista con la DiferenciaSimetrica de conjuntos de l1 con l2
     */
    public ListaCD<Integer> getDiferenciaSimetrica() {

        return null;
    }

    private ListaCD<Integer> crearListaRandom(int veces, int n, int m) {
        ListaCD<Integer> l = new ListaCD();
        while (veces-- > 0) {
            int valorEntero = (int) (Math.floor(Math.random() * (n - m + 1) + m));  // Valor entre M y N, ambos incluidos.
            l.insertarFin(valorEntero);
        }
        return l;
    }

    @Override
    public String toString() {
        return ":)";
    }

}
