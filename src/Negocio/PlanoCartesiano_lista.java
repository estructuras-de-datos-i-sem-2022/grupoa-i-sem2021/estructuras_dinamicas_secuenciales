/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Punto;
import java.util.Iterator;
import java.util.Random;
import util.ufps.colecciones_seed.ListaCD;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * Clase permite crear una colección de puntos de manera aleatoria
 * @author madar
 */
public class PlanoCartesiano_lista {
    
    
    private ListaCD<Punto> myPtos;

    public PlanoCartesiano_lista() {
    }
    
    
    public PlanoCartesiano_lista(int cantidadPuntos) {
        try
        {
            this.myPtos=new ListaCD();
            this.crearPuntos_Aleatorios(cantidadPuntos);
            
        }catch(Exception e)
        {
            System.err.println("No se puede crear los puntos-Cantidad inválida");
        
        }
        
        
    }
    
    private void crearPuntos_Aleatorios(int cantidadPuntos)
    {
    
        for(int i=0;i<cantidadPuntos;i++)
        {
         float x=(float)(Math.random()*200)+1;
         float y=(float)(Math.random()*200)+1;
         Punto nuevoPunto=new Punto(x,y);
         this.myPtos.insertarFin(nuevoPunto);
        }
    }

    @Override
    public String toString() {
        String str="";
        Iterator<Punto> it=this.myPtos.iterator();
        while(it.hasNext())
        {
             Punto myPunto=it.next();
             str+="("+myPunto.getX()+","+myPunto.getY()+")\t";
        }
        return str;
    }
    
    
    
}
