/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import util.ufps.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class PruebaListaCD {

    public static void main(String[] args) {
        probarInsertarInicio();
        probarInsertarFin();
        probarBorrar();
    }

    private static void probarBorrar() {
        ListaCD<Integer> myLista = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            myLista.insertarFin(i);
        }

        String orig = myLista.toString();
        int e1 = myLista.eliminar(0);
        int e_ultimo = myLista.eliminar(myLista.getTamanio() - 1);
        String nueva = myLista.toString();
        String mensaje = "Lista original:" + orig + "\n borrados:" + e1 + "," + e_ultimo + "\n Lista actualizada:" + nueva;
        JOptionPane.showMessageDialog(null, mensaje, "Borrado", JOptionPane.INFORMATION_MESSAGE);

    }

    private static void probarInsertarInicio() {
        ListaCD<Integer> myLista = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            myLista.insertarInicio(i);
        }
        JOptionPane.showMessageDialog(null, myLista.toString(), "Insertar-Inicio-Mi lista es:", JOptionPane.WARNING_MESSAGE);

    }

    private static void probarInsertarFin() {
        ListaCD<Integer> myLista = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            myLista.insertarFin(i);
        }
        JOptionPane.showMessageDialog(null, myLista.toString(), "Insertar-fin-Mi lista es:", JOptionPane.WARNING_MESSAGE);

    }
}
