/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Iterator;
import util.ufps.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class PruebaIterador {

    public static void main(String[] args) {

       // problemaEficiencia();
        solucionEficiencia1();
        solucionEficiencia2();
        
    }
    
    /**
     * Solución usando iteradores con foreach
     */
    private static void solucionEficiencia1() {
        int n = 1000000;
        int inicio = 10;
        int fin = 30;
        ListaCD<Integer> myLista = crearListaRandom(n, inicio, fin);
        int suma = 0;
        for (int dato : myLista) {
            suma += dato;
        }
        System.out.println("Mi algoritmo con iterador (foreach),lista tiene :" + n + " elementos y su suma es:" + suma);
    }

    /**
     * Solución usando iteradores a través de la clase Iterator
     */
    private static void solucionEficiencia2() {
        int n = 1000000;
        int inicio = 10;
        int fin = 30;
        ListaCD<Integer> myLista = crearListaRandom(n, inicio, fin);
        int suma = 0;
        Iterator<Integer> it=myLista.iterator();
        while (it.hasNext()) {
            int dato=it.next();
            suma += dato;
        }
        System.out.println("Mi algoritmo con iterador,lista tiene :" + n + " elementos y su suma es:" + suma);
    }
    
    private static void problemaEficiencia() {
        int n=100000;
        int inicio=10;
        int fin=30;
        ListaCD<Integer> myLista=crearListaRandom(n, inicio, fin);
        int suma=0;
        for(int i=0;i<myLista.getTamanio();i++)
            suma+=myLista.get(i);
        System.out.println("La myLista.get(i);ista tiene :"+n+" elementos y su suma es:"+suma);
    }

    /**
     * Tomado de :
     * http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
     *
     * @param veces cantidad de elementos
     * @param n inicio del random
     * @param m fin del random
     * @return Una lista con elementos randomicos de n hasta m
     */
    private static ListaCD<Integer> crearListaRandom(int veces, int n, int m) {
        ListaCD<Integer> l = new ListaCD();
        while (veces-- > 0) {
            int valorEntero = (int) (Math.floor(Math.random() * (n - m + 1) + m));  // Valor entre M y N, ambos incluidos.
            l.insertarFin(valorEntero);
        }
        return l;
    }
}
