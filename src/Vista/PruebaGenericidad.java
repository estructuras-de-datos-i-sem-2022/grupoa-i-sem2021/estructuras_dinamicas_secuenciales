/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;

/**
 *
 * @author madar
 */
public class PruebaGenericidad {
    
    public static void main(String[] args) {
        String v1[]={"abigail", "andres","jose", "kevin","ana"}; //Caso del String
        Punto v2[]={new Punto(1,2), new Punto(3,1),new Punto(1,0)};
        Integer v3[]={9,5,2,-10};
        
        ordenar(v1);
        ordenar(v2);
        ordenar(v3);
        
        imprimir(v1);
        imprimir(v2);
        imprimir(v3);
    }
    
    //Método genéricos:
    //¿Qué necesita el método para su correcto funcionamiento?
    /*
            1. El parámetro del método sea un vector(arreglo) de objetos
            2. A nivel del elemento --> debe tener método toString
            T --> Object
    */
    public static <T> void imprimir(T vector[])
    {
    
        String msg="Los elementos son: ";
        for(T dato: vector)
                 msg+= dato.toString()+"\t";
        
        System.out.println(msg);
    }
    
    //Ordenar:
    public static <T> void  ordenar(T vector[]) {

        int j = 0;
        for (int i = 1; i < vector.length; i++) {
            T clave = vector[i];
            j = i - 1;
            while (j >= 0 && (((Comparable)vector[j]).compareTo(clave) == 1)) {
                T aux = vector[j + 1];
                vector[j + 1] = vector[j];
                vector[j] = aux;
                j = j - 1;
            }

        }
    }


    
    
}
