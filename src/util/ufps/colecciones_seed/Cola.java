/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

/**
 * Cola implementada usando ListaCD
 * @author madarme
 */
public class Cola<T> {

    private ListaCD<T> myLista = new ListaCD();

    public Cola() {
    }

    public void enColar(T info) {
        this.myLista.insertarFin(info);
    }

    public T deColar() {
        if (this.myLista.esVacia()) {
            throw new RuntimeException("Cola vacía , no se puede deColar");
        }
        return this.myLista.eliminar(0);
    }

    public boolean esVacio() {
        return this.myLista.esVacia();
    }

    public int getTamanio() {
        return this.myLista.getTamanio();
    }
}
